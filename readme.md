# BayesianInference

Julia interface to different exixting MCMC samplers for Bayesian inference.



## Files

* **`BayesianInference.jl`**: Function `BayesianInference` to be called as an interface to different exixting MCMC samplers

* **`TransPar.jl`**: Parameter transformation from one- and two-sided bounded domains to the real axis.

* **`BayesianInference_Test.jl`**: Script to test the interface`.




## Functions

### BayesianInference

Mandatory arguments:
* `logpost`: log posterior function taking a parameter vector `par` as its argument
* `par_ini`: `Float64` vector of initial parameter values
* `parnames`: `String` vector of names of parameters `par_ini`

Optional arguments (for defaults see source code):
* `Trans`: vector with elements of type `ParTrans` containing parameter transformations to be applied for unconstrained inference of transformed parameters
* `MCMCalg`: Sampling algorithm, currently available choices:

  `AdvancedHMC`: Hamiltonian Monte Carlo algorithm from  `AdvancedHMC.jl`
  
  `DynamicHMC`: Hamiltonian Monte Carlo algorithm from  `DynamicHMC.jl`
  
  `am`: adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`
  
  `ram`: robust adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`
  
  `:asm`: adaptive scaling Metropolis algorithm from  `AdaptiveMCMC.jl`
  
  `aswam`: adaptive scaling within adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`
  
* `AutoDiff`: Algorithm for automatic differentiation. Currently `ForwardDiff`, `PolyesterForwardDiff` (only for AdvancedHMC), `Zygote` or `ReverseDiff`
* `n_samples`: number of Markov chain samples to collect
* `n_adapt`: number of samples for adaptation and burn-in (if supported by interfaced package)
* `thin`: thinning of Markov chain for non-HMC algorithms
* `n_leapfrog`: number of leapfrog steps for AdvancedHMC, the default value of zero indicates no-u-turn sampling (NUTS)

Return value:
* Markov chain sample as an object of type `MCMCChains.Chains` (see `Statistics.mean(sample)`, `String.(names(sample))`, `Array(sample)`, `Plots.plot(sample)`)

