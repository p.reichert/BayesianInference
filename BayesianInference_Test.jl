## ============================================================================
##
## File: BayesianInference_Test.jl
##
## Test script for interface function to MCMC algorithms for Bayesian Inference
##
## creation:          July 24    , 2023 -- Peter Reichert
## last modification: July 10    , 2024 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ============================================================================


# Activate, instatiate and load packages:
# =======================================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

import Plots

include("BayesianInference.jl")


# Set algorithmic parameters:
# ===========================

AutoDiffs = ["ForwardDiff","PolyesterForwardDiff","Zygote","ReverseDiff"]
# AutoDiffs = ["ForwardDiff"]

MCMCalgs = ["AdvancedHMC","DynamicHMC","am","ram","asm","aswam"]
# MCMCalgs = ["AdvancedHMC"]

n_samples = 1000


# Define initial parameter values and log density:
# ================================================

par      = [1.0,1.0,1.0,1.0]
parnames = ["p1","p2","p3","p4"]

means    = [1.0,2.0,3.0,4.0]
sds      = [0.2,0.4,0.6,0.8]

# Trans = missing

Trans = Array{TransPar}(undef,4)
Trans[1] = TransParUnbounded()
Trans[2] = TransParLowerBound(0.0)
Trans[3] = TransParUpperBound(10.0)
Trans[4] = TransParInterval(0.0,10.0)

function logdensity(par,means,sds)
   return - 0.5*sum(((par.-means)./sds).^2)
end

logdensity(par) = logdensity(par,means,sds)


# Sample and plot for all algorithms:
# ===================================

for MCMCalg in MCMCalgs

   if MCMCalg == "AdvancedHMC"

      for AutoDiff in AutoDiffs

          @time sample = BayesianInference(logdensity,par,parnames;
                                           Trans      = Trans,
                                           AutoDiff   = AutoDiff,
                                           MCMCalg    = MCMCalg,
                                           n_samples  = n_samples)

          p = Plots.plot(sample;seriestype=(:traceplot,:histogram),Title=string("algorithm: :",MCMCalg,"_",AutoDiff," - NUTS"))
          Plots.savefig(p,string("chains_",MCMCalg,"_NUTS","_",AutoDiff,".pdf"))

          n_leapfrog   = 10
          initial_step = 0.3
          @time sample = BayesianInference(logdensity,par,parnames;
                                           Trans        = Trans,
                                           AutoDiff     = AutoDiff,
                                           MCMCalg      = MCMCalg,
                                           n_samples    = n_samples,
                                           n_leapfrog   = n_leapfrog,
                                           initial_step = initial_step)
          p = Plots.plot(sample;seriestype=(:traceplot,:histogram),Title=string("algorithm: :",MCMCalg,"_",AutoDiff," - ",n_leapfrog," leapfrog steps"))
          Plots.savefig(p,string("chains_",MCMCalg,"_",n_leapfrog,"_",AutoDiff,".pdf"))

      end

   elseif MCMCalg == "DynamicHMC"

      for AutoDiff in AutoDiffs

          if AutoDiff != "PolyesterForwardDiff"

              @time sample = BayesianInference(logdensity,par,parnames;
                                               Trans      = Trans,
                                               AutoDiff   = AutoDiff,
                                               MCMCalg    = MCMCalg,
                                               n_samples  = n_samples)

              p = Plots.plot(sample;seriestype=(:traceplot,:histogram),Title=string("algorithm: :",MCMCalg,"_",AutoDiff))
              Plots.savefig(p,string("chains_",MCMCalg,"_",AutoDiff,".pdf"))

          end

      end

   else

      @time sample = BayesianInference(logdensity,par,parnames;
                                       Trans      = Trans,
                                       MCMCalg    = MCMCalg,
                                       n_samples  = n_samples)

      p = Plots.plot(sample;seriestype=(:traceplot,:histogram),Title=string("algorithm: :",MCMCalg))
      Plots.savefig(p,string("chains_",MCMCalg,".pdf"))

   end

end

